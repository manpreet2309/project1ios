//
//  InterfaceController.swift
//  project1 WatchKit Extension
//
//  Created by MacStudent on 2019-10-16.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBOutlet weak var responseLabel: WKInterfaceLabel!
    
    
    @IBAction func reply() {
        
        let suggestionsList = ["pizza", "pie", "cake"]
        
        presentTextInputController(withSuggestions: suggestionsList, allowedInputMode: .plain) {
            
            (results) in
            // Put your code here
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.responseLabel.setText(userResponse)

        }

    }
}
}
